# Rescramble base
if [[ "$POLYVERSE_POLYMORPHIC_LINUX_KEY" != "" ]]; then \
  echo "Rescrambling all packages on this image..." && \
  echo "Using Scrambling key: $POLYVERSE_POLYMORPHIC_LINUX_KEY" && \
  curl https://repo.polyverse.io/install.sh | sh -s "$POLYVERSE_POLYMORPHIC_LINUX_KEY" && \
  yum reinstall -y \* && \
  echo "Scrambling complete."; \
fi
